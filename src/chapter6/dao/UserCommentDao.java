//TopServletを大元にCommentServiceから呼び出される
//SQLを構築しツイート内容を返すモデルの役割を担うプログラム

package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.UserComment;
import chapter6.exception.SQLRuntimeException;

public class UserCommentDao {

	//CommentServiceクラスのList<UserMessage> selectメソッドから呼ばれる
	//DBにSQLを投げユーザーメッセージをリストに格納する
	public List<UserComment> select(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("    comments.id as id, ");
			sql.append("    comments.text as text, ");
			sql.append("    comments.user_id as user_id, ");
			sql.append("    comments.message_id as message_id, ");
			sql.append("    users.account as account, ");
			sql.append("    users.name as name, ");
			sql.append("    comments.created_date as created_date ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<UserComment> comments = toComment(rs);

			if (comments.isEmpty()) {
				return null;
			} else {
				return comments;
			}

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//同クラスのList<UserComment> selectメソッドから呼び出される
	//sqlの結果からbeanパッケージのUserCommentオブジェクト(comments)に値を格納する
	//commentsにcommentの情報を格納しその値を返す
	private List<UserComment> toComment(ResultSet rs) throws SQLException {

		List<UserComment> comments = new ArrayList<UserComment>();
		try {
			while (rs.next()) {
				UserComment comment = new UserComment();
				comment.setId(rs.getInt("id"));
				comment.setText(rs.getString("text"));
				comment.setUserId(rs.getInt("user_id"));
				comment.setMessageId(rs.getInt("message_id"));
				comment.setAccount(rs.getString("account"));
				comment.setName(rs.getString("name"));
				comment.setCreatedDate(rs.getTimestamp("created_date"));

				comments.add(comment);
			}
			return comments;
		} finally {
			close(rs);
		}
	}
}