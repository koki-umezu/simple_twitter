//MessageServletを大元にMessageServiceから呼び出される
//SQLを構築しツイートを登録するモデルの役割を担うプログラム

package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Message;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class MessageDao {

	//MessageServiceクラスのinsertメソッドから呼び出される
	//beanに格納されているユーザーidとツイートを基にsqlを構築し登録処理を行う
	public void insert(Connection connection, Message message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO messages ( ");
			sql.append("    user_id, ");
			sql.append("    text, ");
			sql.append("    created_date, ");
			sql.append("    updated_date ");
			sql.append(") VALUES ( ");
			sql.append("    ?, "); // user_id
			sql.append("    ?, "); // text
			sql.append("    CURRENT_TIMESTAMP, "); // created_date
			sql.append("    CURRENT_TIMESTAMP "); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, message.getUserId());
			ps.setString(2, message.getText());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//MessageServiceクラスのdeleteメソッドから呼び出される
	//top.jspから受け取ったmessageIdを参照しデータベースから該当するmessageを削除する
	public void delete(Connection connection, int messageId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM messages WHERE id = ? ");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, messageId);

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//MessageServiceクラスのupdateメソッドから呼び出される
	//top.jspから受け取ったmessageIdを参照しデータベースから該当するmessageを更新する
	public void update(Connection connection, Message message) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE messages SET ");
			sql.append("    text = ?, ");
			sql.append("    updated_date = CURRENT_TIMESTAMP ");
			sql.append("WHERE id = ? ");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, message.getText());
			ps.setInt(2, message.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//MessageServiceクラスのList<UserMessage> selectMessage(String messageId)メソッドから呼び出される
	//edit.jspから受け取ったmessageIdを参照しデータベースから該当するmessageを参照する
	public Message selectMessage(Connection connection, int messageId, int userId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM messages WHERE id = ? ");
			sql.append("AND user_id = ? ");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, messageId);
			ps.setInt(2, userId);

			ResultSet rs = ps.executeQuery();

			List<Message> messages = toMessage(rs);

			if(messages.isEmpty()) {
				return null;
			}else {
				return messages.get(0);
			}

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<Message> toMessage(ResultSet rs) throws SQLException {

		List<Message> messages = new ArrayList<Message>();
		try {
			while (rs.next()) {
				Message message = new Message();
				message.setId(rs.getInt("id"));
				message.setText(rs.getString("text"));
				message.setUserId(rs.getInt("user_id"));
				message.setCreatedDate(rs.getTimestamp("created_date"));

				messages.add(message);
			}
			return messages;
		} finally {
			close(rs);
		}
	}
}

