//TopServletやMessageServletから呼び出される
//ツイート内容をコミットもしくはロールバックするモデルの役割を担うプログラム

package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

	//MessageServiceクラスのdoPostメソッドから呼び出される
	//MessageDaoクラスのinsertメソッドを呼び出す
	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//DeleteMessageServletクラスのdoPostメソッドから呼び出される
	//MessageDaoクラスのdeleteメソッドを呼び出す
	public void delete(int messageId) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().delete(connection, messageId);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//EditServletクラスのdoPostメソッドから呼び出される
	//MessageDaoクラスのupdateメソッドを呼び出す
	public void update(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().update(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//EditServletクラスのdoGetメソッドから呼び出される
	//MessageDaoクラスのselectMessageメソッドを呼び出す
	public Message selectMessage(String messageId, int userId) {

		Connection connection = null;
		try {
			connection = getConnection();

			Message message = new MessageDao().selectMessage(connection, Integer.parseInt(messageId), userId);
			commit(connection);

			return message;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//TopServletから呼び出される
	//UserMessageDaoクラスのselectメソッドを呼び出す
	//DB接続（DBUtilから）をしmessages（ツイート内容）を返す
	public List<UserMessage> select(String userId, String start, String end) {
		final int LIMIT_NUM = 1000;

		Connection connection = null;
		try {
			connection = getConnection();

			Integer id = null;
			if (!StringUtils.isBlank(userId)) {
				id = Integer.parseInt(userId);
			}

			String startDate = null;
			String endDate = null;

			//TopServletから受け取った日付データが存在しない場合はデフォルトを使用する
			//存在する場合はミリ秒まで付け足す
			if (!StringUtils.isBlank(start)) {
				startDate = start + " 00:00:00";
			} else {
				startDate = ("2020-01-01 00:00:00");
			}

			if (!StringUtils.isBlank(end)) {
				endDate = end + " 23:59:59";
			} else {
				java.sql.Date tmpEndDate = new java.sql.Date(0);
				tmpEndDate.setTime(System.currentTimeMillis());
				endDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tmpEndDate);
			}

			List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, startDate, endDate);
			commit(connection);

			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
