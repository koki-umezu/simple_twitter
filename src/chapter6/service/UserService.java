//SignUpServletやSettingServletから呼び出される
//ユーザー情報をコミットもしくはロールバックするモデルの役割を担うプログラム

package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.dao.UserDao;
import chapter6.utils.CipherUtil;

public class UserService {

	//SignupServletから呼び出される
	//UserDaoクラスのinsertメソッドを呼び出す
	public void insert(User user) {

		Connection connection = null;
		try {
			// パスワード暗号化
			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			connection = getConnection();
			new UserDao().insert(connection, user);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//SettingServletのdoPostメソッドから呼び出される
	//UserDaoクラスのupdateメソッドを呼び出す
	public void update(User user) {

		Connection connection = null;
		try {
			// パスワード暗号化
			if(!StringUtils.isBlank(user.getPassword())) {
				String encPassword = CipherUtil.encrypt(user.getPassword());
				user.setPassword(encPassword);
			}

			connection = getConnection();
			new UserDao().update(connection, user);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//LoginServletから呼び出される
	//UserDaoクラスのselectメソッドを呼び出しユーザー情報を返す
	public User select(String accountOrEmail, String password) {

		Connection connection = null;
		try {
			// パスワード暗号化
			String encPassword = CipherUtil.encrypt(password);

			connection = getConnection();
			User user = new UserDao().select(connection, accountOrEmail, encPassword);
			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//SettingServletから呼び出される
	//UserDaoクラスのselectメソッドを呼び出しユーザー情報を返す
	public User select(int userId) {

		Connection connection = null;
		try {
			connection = getConnection();
			User user = new UserDao().select(connection, userId);
			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//SignUpServletやSettingServletから呼び出される
	//UserDao().selectにアカウント情報を送る
	//エラーが出た場合はロールバックする
	public User select(String account) {

	    Connection connection = null;
	    try {
	        connection = getConnection();
	        User user = new UserDao().select(connection, account);
	        commit(connection);

	        return user;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}

}