//setting.jspから受け取った情報をモデルに渡すコントローラーの役割

package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.service.UserService;

//urlの決定
@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {

	//getされた時の処理
	//セッションの取得
	//現在ログインしているユーザーの情報を取得
	//UserServiceクラスのselectメソッドを呼び出しuserオブジェクトに値を格納
	//ユーザー情報をセットしsetting.jspを呼び出す
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");

		User user = new UserService().select(loginUser.getId());

		request.setAttribute("user", user);
		request.getRequestDispatcher("setting.jsp").forward(request, response);
	}

	//postされた時の処理
	//セッションの取得
	//postの値を引数に同クラスのgetUserメソッドを呼び出し値をuserに格納する
	//同クラスのisValidメソッドを呼び出し入力情報を判別する
	//UserService().selectメソッドを呼び出し、同一アカウント名がある場合はエラー表示する
	//UserServiceクラスのupdateメソッドを呼び出し処理が正常に行われるか判別する
	//処理が正常に行われない場合エラーを表示する
	//現在のログイン情報をセッションに格納し前の画面に戻る
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		User user = getUser(request);
		User loginUser = (User) session.getAttribute("loginUser");

		if (isValid(user, errorMessages)) {
			try {
				new UserService().update(user);
			} catch (NoRowsUpdatedRuntimeException e) {
				errorMessages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
			}
		}

		if (errorMessages.size() != 0) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("user", user);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
			return;
		}

		session.setAttribute("loginUser", user);
		response.sendRedirect("./");

	}

	//同クラスのdoPostメソッドから呼び出される
	//Userクラスを呼び出しpostの値を格納する
	private User getUser(HttpServletRequest request) throws IOException, ServletException {

		User user = new User();
		user.setId(Integer.parseInt(request.getParameter("id")));
		user.setName(request.getParameter("name"));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setEmail(request.getParameter("email"));
		user.setDescription(request.getParameter("description"));
		return user;
	}

	//同クラスのdoPostメソッドから呼び出される
	//userに格納されている情報を取得し入力内容を判別する
	private boolean isValid(User user, List<String> errorMessages) {

		String name = user.getName();
		String account = user.getAccount();
		String password = user.getPassword();
		String email = user.getEmail();
		int id = user.getId();
		User newUser = new UserService().select(account);

		if (!StringUtils.isEmpty(name) && (20 < name.length())) {
			errorMessages.add("名前は20文字以下で入力してください");
		}
		if (StringUtils.isEmpty(account)) {
			errorMessages.add("アカウント名を入力してください");
		} else if (20 < account.length()) {
			errorMessages.add("アカウント名は20文字以下で入力してください");
		}

		if (newUser != null) {
			if (newUser.getId() != id) {
				errorMessages.add("すでに存在するアカウントです");
			}
		}
		if (!StringUtils.isEmpty(email) && (50 < email.length())) {
			errorMessages.add("メールアドレスは50文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}