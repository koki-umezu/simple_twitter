//top.jspから受け取った情報をモデルに渡すコントローラーの役割

package chapter6.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.User;
import chapter6.beans.UserComment;
import chapter6.beans.UserMessage;
import chapter6.service.CommentService;
import chapter6.service.MessageService;

//urlを決定
@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {

	//getされた時の処理
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		boolean isShowMessageForm = false;

		//セッションからログイン状態を判別し、ユーザーデータの取得とメッセージフォームの表示
		User user = (User) request.getSession().getAttribute("loginUser");
		if (user != null) {
			isShowMessageForm = true;
		}
		String userId = request.getParameter("user_id");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");

		//MessageServiceのselectメソッドを呼び出しリストにmessagesを格納
		List<UserMessage> messages = new MessageService().select(userId, startDate, endDate);

		//メッセージを取り出し、CommentServiceクラスのselectメソッドを呼び出し値を格納
		List<UserComment> comments = new CommentService().select();

		//top.jspの入力フォームで受け取ったツイートをセットする
		request.setAttribute("startDate", startDate);
		request.setAttribute("endDate", endDate);
		request.setAttribute("comments", comments);
		request.setAttribute("messages", messages);
		request.setAttribute("isShowMessageForm", isShowMessageForm);
		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}
}
