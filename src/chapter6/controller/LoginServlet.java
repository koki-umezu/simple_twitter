//login.jspから受け取った情報をモデルに渡すコントローラーの役割を持つプログラム

package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;
import chapter6.service.UserService;

//urlの決定
@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {

	//getされた時の処理
	//login.jspを呼び出す
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	//postされた時の処理
	//UserServiceクラスのselectメソッドを呼び出しuserに格納
	//戻り値のユーザー情報が無い場合エラーを返す
	//セッションにログインユーザーの情報を格納し、前の画面に戻る
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		String accountOrEmail = request.getParameter("accountOrEmail");
		String password = request.getParameter("password");

		User user = new UserService().select(accountOrEmail, password);
		if (user == null) {
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("ログインに失敗しました");
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}

		HttpSession session = request.getSession();
		session.setAttribute("loginUser", user);
		session.setAttribute("errorMessages", null);
		response.sendRedirect("./");
	}
}
