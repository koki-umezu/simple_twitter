//top.jspのツイートする領域から受け取った情報をモデルに渡すプログラム

package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.User;
import chapter6.service.MessageService;

//urlの決定
@WebServlet(urlPatterns = { "/message" })
public class MessageServlet extends HttpServlet {

	//postされた時の処理
	//postのパラメータからテキストを取得しtextに格納
	//同クラスのisValidを呼び出しエラーを判別
	//エラーの場合はエラーメッセージを表示し前のページに戻る
	//beansパッケージのMessageオブジェクトにツイート内容を格納
	//messageにidを追加
	//MessageServiceクラスのinsertメソッドを呼び出し前の画面に戻る
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		String text = request.getParameter("text");
		if (!isValid(text, errorMessages)) {
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		Message message = new Message();
		message.setText(text);

		User user = (User) session.getAttribute("loginUser");
		message.setUserId(user.getId());

		new MessageService().insert(message);
		response.sendRedirect("./");
	}

	//同クラスのdoPostメソッドから呼び出される
	//textに格納されている情報を取得し入力内容を判別する
	private boolean isValid(String text, List<String> errorMessages) {

		if (StringUtils.isBlank(text)) {
			errorMessages.add("メッセージを入力してください");
		} else if (140 < text.length()) {
			errorMessages.add("140文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}
