//top.jspで削除したい情報をモデルに渡すコントローラーの役割
package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.service.MessageService;

//urlの決定
@WebServlet(urlPatterns = { "/deleteMessage" })
public class DeleteMessageServlet extends HttpServlet{

	//postされた時の処理
	//メッセージのIDを取得しmessageIdに格納
	//MessageServiceクラスのdeleteメソッドを呼び出しトップに戻る
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		int messageId =Integer.parseInt(request.getParameter("messageId"));

		new MessageService().delete(messageId);

		response.sendRedirect("./");

		}
	}


