//edit.jsp受け取った情報をモデルに渡すコントローラーの役割
package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.User;
import chapter6.service.MessageService;

//urlの決定
@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

	//getされた時の処理
	//getのパラメーターが正常出ない場合エラーを表示
	//メッセージのツイート内容をメッセージIDを基に取得
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> errorMessages = new ArrayList<String>();

		//セッションからログイン状態を判別し、ユーザーデータの取得とメッセージフォームの表示
		User user = (User) request.getSession().getAttribute("loginUser");

		Message message = null;
		String messageId = request.getParameter("message_id");
		int userId = user.getId();

		if (!StringUtils.isBlank(messageId) && messageId.matches("^[0-9]+$")) {
			message = new MessageService().selectMessage(messageId , userId);
		}

		if (message == null) {
			errorMessages.add("不正なパラメータが入力されました");
		}

		if (errorMessages.size() != 0) {
			session.setAttribute("errorMessages", errorMessages);
			request.setAttribute("user", user);
			response.sendRedirect("./");
			return;
		}

		request.setAttribute("message", message);
		request.getRequestDispatcher("/edit.jsp").forward(request, response);

	}

	//postされた時の処理
	//edit.jspからテキストの内容を受け取り、DBの情報を更新する処理
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();

		String text = request.getParameter("text");
		int messageId = Integer.parseInt(request.getParameter("id"));

		Message message = new Message();
		message.setText(text);
		message.setId(messageId);

		if (!isValid(text, errorMessages)) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("message", message);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
			return;
		}

		new MessageService().update(message);
		response.sendRedirect("./");
	}

	//同クラスのdoPostメソッドから呼び出される
	//textに格納されている情報を取得し入力内容を判別する
	private boolean isValid(String text, List<String> errorMessages) {

		if (StringUtils.isBlank(text)) {
			errorMessages.add("入力してください");
		} else if (140 < text.length()) {
			errorMessages.add("140文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}

		return true;
	}

}
