$(function() {
	//確認ダイヤログを表示する処理

	$('.delete, .message, .comment').on('click', function() {
		console.log($(this).attr('class'));
		let messageLog;
		switch($(this).attr('class')){
		case 'delete':
			messageLog ='つぶやきを削除してもよろしいですか？';
			break;
		case 'message':
			messageLog ='"' + $('.send-message textarea[name="text"]').val() + '"' + 'の内容で投稿してよろしいですか？';
			break;
		case 'comment':
			messageLog ='"' + $(this).prevAll("textarea").val() + '"' + 'の内容で投稿してよろしいですか？';
			break;
		}

		let result = confirm(messageLog);

		if(result){
			return true;
		}else{
			return false;
		}
	});

});