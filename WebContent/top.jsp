<%--TOP画面のviewを担当するjsp --%>

<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>簡易Twitter</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>


<body>
	<div class="main-contents">

		<%--
			ヘッダー領域
			loginUserの値が空か判別し表示を変える。
			TopServletでセッションから値を取る
		 --%>
		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
				<a href="signup">登録する</a>
			</c:if>
			<c:if test="${ not empty loginUser }">
				<a href="./">ホーム</a>
				<a href="setting">設定</a>
				<a href="logout">ログアウト</a>
			</c:if>
		</div>

		<div class="filter">
			<form action="index.jsp" method="get">
				<span>日付</span> <input type="date" name="startDate" class="date"
					value="<c:out value="${startDate}" />"> <span>～</span> <input
					type="date" name="endDate" class="date"
					value="<c:out value="${endDate}" />"> <input type="submit"
					value="絞込">
			</form>
		</div>

		<%-- ログインされている場合表示する画面 --%>

		<c:if test="${ not empty loginUser }">

			<%-- 登録情報を表示する画面 --%>

			<div class="profile">
				<div class="name">
					<h2>
						<c:out value="${loginUser.name}" />
					</h2>
				</div>
				<div class="account">
					@
					<c:out value="${loginUser.account}" />
				</div>
				<div class="description">
					<c:out value="${loginUser.description}" />
				</div>
			</div>

			<%-- エラーメッセージが存在する場合表示する画面 --%>

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
							<li><c:out value="${errorMessage}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<%--
				書き込み(ツイートする)領域
				フォームからpostでデータを送信する
				TopServletでセットを行う
			--%>

			<div class="send-message">
				<div class="form-area">
					<c:if test="${ isShowMessageForm }">
						<form action="message" method="post">
							いま、どうしてる？<br />
							<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
							<br /> <input type="submit" value="つぶやく" class="message">（140文字まで）
						</form>
					</c:if>
				</div>
			</div>

		</c:if>

		<%-- ツイートされた内容を表示する領域 --%>
		<div class="messages">
			<c:forEach items="${messages}" var="message">
				<div class="message">
					<div class="account-name">
						<span class="account"> <a
							href="./?user_id=<c:out value="${message.userId}" />"> <c:out
									value="${message.account}" /></a>
						</span> <span class="name"><c:out value="${message.name}" /></span>
					</div>

					<div class="text">
						<c:out value="${message.text}" />
					</div>

					<div class="date">
						<fmt:formatDate value="${message.createdDate}"
							pattern="yyyy/MM/dd HH:mm:ss" />
					</div>
				</div>

				<c:if test="${message.userId == loginUser.id}">

					<%-- 編集画面を呼び出すボタン --%>
					<div>
						<form action="edit" method="get">
							<input type="hidden" name="message_id"
								value=<c:out value="${message.id}" />> <input
								type="submit" value="編集">
						</form>
					</div>
					<%-- ツイートを削除するボタン --%>
					<div>
						<form action="deleteMessage" method="post" class="delete">
							<input type="hidden" name=messageId
								value=<c:out value="${message.id}" />> <input
								type="submit" value="削除">
						</form>
					</div>
				</c:if>

				<%-- 返信を表示する画面 --%>
				<c:forEach items="${comments}" var="comment">
					<c:if test="${comment.messageId == message.id}">
						<div>
							<div class="account-name">
								<span class="account"><c:out value="${comment.account}" /></span>
								<span class="name"><c:out value="${comment.name}" /></span>
							</div>

							<div class="text">
								<c:out value="${comment.text}" />
							</div>

							<div class="date">
								<fmt:formatDate value="${comment.createdDate}"
									pattern="yyyy/MM/dd HH:mm:ss" />
							</div>
						</div>
					</c:if>
				</c:forEach>

				<%-- 返信するフォームを表示する画面 --%>
				<c:if test="${ not empty loginUser }">
					<div class="send-comments">
						<form action="comment" method="post">
							<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
							<input type="hidden" name=messageId
								value=<c:out value="${message.id}" />> <br /> <input
								type="submit" value="返信" class="comment">（140文字まで）
						</form>
					</div>
				</c:if>

			</c:forEach>
		</div>

		<div class="copyright">Copyright(c)koki_umezu</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script type="text/javascript" src="./js/test.js"></script>
</body>
</html>